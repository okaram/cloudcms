import boto3

dynamodb = boto3.resource('dynamodb')
pagesTable = dynamodb.Table('SimpleCMS.Pages')
pageTypesTable = dynamodb.Table('SimpleCMS.PageTypes')

pagesTable.put_item(
   Item={
        'Name': 'firstPage',
        'Handle': 'hp',
        'Template': '<b>Hello {{ name }}</b>!',
        'Data': '{"name" : "Orlando"}'
    }
)

pageTypesTable.put_item(
    Item={
        'Name': 'home',
        'Schema': '{
    "type":"object",
    "properties": {
        "carousels" : {
            "type": "array",
            "items": [
                {
                    "type": "number"
                }
            ],
            "minItems":1,
            "maxItems":5
        }
    },
    "required": ["carousels"]
}
'
    }
)

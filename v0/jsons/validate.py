import json
from jsonschema import validate, Draft3Validator
import yaml 

data_fname="data1.yml"
schema_fname="schema1.yml"

data=yaml.load(open(data_fname).read())
schema=yaml.load(open(schema_fname).read())
print(data)
print(schema)
print(validate(data,schema))

validator=Draft3Validator(schema)
for error in validator.iter_errors(data):
    print (error)
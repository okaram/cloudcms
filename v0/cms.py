from flask import Flask
import boto3
from jinja2 import Template
import json

dynamodb = boto3.resource('dynamodb')
pagesTable = dynamodb.Table('SimpleCMS.Pages')
pageTypesTable = dynamodb.Table('SimpleCMS.PageTypes')

app = Flask(__name__)

@app.route("/")
def hello():
    return "Hello World!"

@app.route("/render/<pageName>")
def render(pageName):
    item=pagesTable.get_item( Key={'Name':pageName})
    template=Template(item['Item']['template'])
    return template.render(json.loads(item['Item']['data']))

app.run()
